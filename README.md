# 100 Prisoners
The 100 prisoners problem is a mathematical problem in probability theory and combinatorics. In this problem, 100 numbered prisoners must find their own numbers in one of 100 drawers in order to survive. The rules state that each prisoner may open only 50 drawers and cannot communicate with other prisoners. More about 100 prisoner problem --> http://datagenetics.com/blog/december42014/index.html

**Implementation of problem 100 prisoners with data visualization.**

The 100 prisoners problem project presents two approaches for this particular problem.
1.  In file `100_prisoners.py` is presented strategy when every prisoner selects 50 drawers at random. 

In this case the probability of escape is close to 0.

2.  In file `100_prisoners_with_strategy.py` is presented strategy which is based on cycles in permutation. Below how exactly algorithm works:
    * Each prisoner first opens the drawer with his own number.
    * If this drawer contains his number he is done and was successful.
    * Otherwise, the drawer contains the number of another prisoner and he next opens the drawer with this number.
    * The prisoner repeats steps 2 and 3 until he finds his own number or has opened 50 drawers.

Using the cycle-following strategy the prisoners survive in a 31% of cases


