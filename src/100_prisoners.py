import random as rnd
from src import utils as ut
import argparse


def random_choice_of_drawer_without_repeat(list_of_chosen_drawers):
    a = range(1, 101)
    a = [x for x in a if x not in list_of_chosen_drawers]
    chosen_drawer_by_prison = rnd.sample(a, 1)
    return chosen_drawer_by_prison[0]


def hundred_prisoners_random(list_of_drawers):
    max_attempts_number = 50
    for prisoner_number in range(1, 101):
        attempt_number = 1
        list_of_chosen_drawers = []
        while attempt_number <= max_attempts_number + 1:
            if attempt_number == max_attempts_number + 1:
                return 0
            else:
                chosen_draw = random_choice_of_drawer_without_repeat(list_of_chosen_drawers)
                if list_of_drawers[chosen_draw - 1] != prisoner_number:
                    attempt_number = attempt_number + 1
                    list_of_chosen_drawers.append(chosen_draw)
                else:
                    break
    return 1


parser = argparse.ArgumentParser(
    description="Mathematical problem of 100 prisoners that try to escape without any strategy -" \
                "program calculates probability of such escape")
parser.add_argument("--a", required=True, type=int, help="Number of simulations")

args = parser.parse_args()
number_of_simulations = args.a

print(ut.probability_of_escape(number_of_simulations, hundred_prisoners_random))
ut.probability_of_escape_plot(number_of_simulations, hundred_prisoners_random)
