from src import utils as ut
import argparse


def cycles_in_permutation(permutation):
    list_of_cycles = []

    for index in range(len(permutation)):
        cycle_i = []
        if not identify_existing_cycles(index, list_of_cycles) or not list_of_cycles:
            index_original = index
            index_plus_one = index + 1
            cycle_i.append(index_plus_one)
            if index_plus_one == permutation[index]:
                list_of_cycles.append(cycle_i)
            else:
                while index_plus_one != permutation[index_original]:
                    cycle_i.append(permutation[index_original])
                    index_original = permutation[index_original] - 1
        if cycle_i:
            list_of_cycles.append(cycle_i)
    return list_of_cycles


def identify_existing_cycles(index, list_of_cycles):
    for element in list_of_cycles:
        if finding_unique_cycles_in_permutation(element, index):
            return True


def finding_unique_cycles_in_permutation(cycle_in_permutation, index):
    b = index + 1
    a = any(b in cycle_in_permutation for i in cycle_in_permutation)
    return a


def max_cycle_length(cycles):
    return len(max(cycles, key=len))


def hundred_prisoners_with_strategy(list_of_drawers):
    list_of_cycles_in_permutation = cycles_in_permutation(list_of_drawers)
    max_length = max_cycle_length(list_of_cycles_in_permutation)
    if max_length <= 50:
        return 1
    else:
        return 0


parser = argparse.ArgumentParser(
    description="Mathematical problem of 100 prisoners that try to escape with strategy that is based on cycles in" \
                "permutations - program calculates probability of such escape")
parser.add_argument("--a", required=True, type=int, help="Number of simulations")

args = parser.parse_args()
number_of_simulations = args.a

print(ut.probability_of_escape(number_of_simulations, hundred_prisoners_with_strategy))
ut.probability_of_escape_plot(number_of_simulations, hundred_prisoners_with_strategy)
