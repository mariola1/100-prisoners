import random as rnd
import matplotlib.pyplot as plt


def probability_of_escape_plot(number_of_simulations, strategy_name, ):
    x = list(range(1, number_of_simulations))
    y = [probability_of_escape(i, strategy_name) for i in range(1, number_of_simulations)]
    plt.xlabel('Number of simulations')
    plt.ylabel('Probability of escape')
    plt.plot(x, y)
    plt.show()


def randomly_placing_numbers_in_drawers():
    x = rnd.sample(range(1, 101), 100)
    return x


def probability_of_escape(number_of_simulation, strategy_name):
    success = 0
    for i in range(number_of_simulation):
        success += strategy_name(randomly_placing_numbers_in_drawers())
    return round(success / number_of_simulation, 8)
