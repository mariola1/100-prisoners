from unittest import TestCase

from src.utils import randomly_placing_numbers_in_drawers


class Test(TestCase):
    def test_probability_of_escape_plot_valid_length(self):
        """
        Test that probability_of_escape_plot return valid length of data
        """
        result = randomly_placing_numbers_in_drawers()
        self.assertTrue(len(result), 100)

    def test_probability_of_escape_plot_out_of_range(self):
        """
        Test that probability_of_escape_plot not return value out of range
        """
        result = randomly_placing_numbers_in_drawers()
        self.assertNotIn(1000, result)
